package com.favorite.favorite;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public class FavoriteRepository extends CrudeRepository<Favorite, long>{
    list<Favorite> findByJumlahAkses(String jumlahAkses);

    ArrayList<Favorite> findByTerakhirAkses(String terakhirAkses);
}
