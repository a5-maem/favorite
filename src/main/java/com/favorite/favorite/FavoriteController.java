package com.favorite.favorite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class FavoriteController {
    @Autowired
    FavoriteRepository favoriteRepository;

    @RequestMapping(value="/getFavorite", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Favorite> getFavorite() {
        return favoriteRepository.findAll();
    }

}
