package com.favorite.favorite;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Favorite {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String jumlahAkses;
    private String terakhirAkses;
    private String restaurant;
    private String picture;

    protected Favorite() {}

    public Favorite(String jumlahAkses, String terakhirAkses, String restaurant, String picture) {
        this.jumlahAkses = jumlahAkses;
        this.terakhirAkses = terakhirAkses;
        this.restaurant = restaurant;
        this.picture=picture;
    }

    @Override
    public String toString() {
        return String.format(
                "Favorite[id=%d, jumlahAkses='%s', terakhirAkses='%s']",
                id, jumlahAkses, terakhirAkses
        );
    }

    public String getJumlahAkses() {
        return jumlahAkses;
    }

    public String getTerakhirAkses() {
        return terakhirAkses;
    }

    public Long getId() {
        return id;
    }
}
